#include "LinkCryptoLib"
#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "des.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>

using namespace std;
string encryptAES(string);
string decryptAES(string);
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

//this is a program that will test your library installation
//use it also to understand one of many ways to work with the library
//this program will ask you for an input, encrypt it using AES encryption and print the resault of the plaintext, ciphertext and finally the decrypted text
void main()
{
	string plainText, cipherText, decryptedText;
	getline(cin, plainText);
	cout << "original plain text is: " + plainText << endl;

	cipherText = encryptAES(plainText);
	cout << "cipher text is: " + cipherText << endl;

	decryptedText = decryptAES(cipherText);
	cout << "decrypted text is: " + decryptedText << endl;

	system("pause");
};

string encryptAES(string plainText){

	string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

string decryptAES(string cipherText){

	string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}